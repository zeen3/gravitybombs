"use strict";
const bombs = document.querySelector('canvas#gravitybombs');
if (bombs instanceof HTMLCanvasElement) {
    bombs.width = window.innerWidth;
    bombs.height = window.innerHeight;
    bombs.width = bombs.height = 1;
    bombs.style.cursor = 'crosshair';
    bombs.style.position = 'absolute';
    bombs.style.left = bombs.style.top = '0px';
    try {
        let osc = bombs.transferControlToOffscreen();
        const m = new Worker('./math.mjs');
        const d = new Worker('./draw.mjs');
        bombs.addEventListener('click', ev => m.postMessage({
            type: 'click',
            x: ev.clientX,
            y: ev.clientY,
            dpr: devicePixelRatio
        }));
        addEventListener('resize', () => d.postMessage({
            type: 'resize',
            width: window.innerWidth,
            height: window.innerHeight,
            ratio: devicePixelRatio
        }));
        const dvor = (e) => {
            const buf = new ArrayBuffer(16);
            const { beta, gamma } = e;
            const f64 = new Float64Array(buf);
            f64[0] = (gamma || 0) / 32;
            f64[1] = (beta || 0) / 32;
            m.postMessage({ type: 'motion', f64 }, [buf]);
        };
        const nild = new Float64Array(2);
        addEventListener('message', ({ data }) => data.type === 'orientation' && (data.orient ?
            (removeEventListener('deviceorientation', dvor, true), addEventListener('deviceorientation', dvor, true)) :
            (removeEventListener('deviceorientation', dvor, true), m.postMessage({ type: 'motion', f64: nild }))));
        osc.height = innerHeight * devicePixelRatio;
        osc.width = innerWidth * devicePixelRatio;
        d.postMessage({
            type: 'osc',
            bomber: osc,
            dpr: devicePixelRatio
        }, [osc]);
        const mc = new MessageChannel();
        d.postMessage({
            type: 'port',
            port: mc.port1
        }, [mc.port1]);
        m.postMessage({
            type: 'port',
            port: mc.port2
        }, [mc.port2]);
        d.postMessage({
            type: 'langs',
            langs: (navigator.languages && navigator.languages.length) ? navigator.languages : ['en']
        });
        addEventListener('languagechange', () => d.postMessage({
            type: 'langs',
            langs: (navigator.languages && navigator.languages.length) ? navigator.languages : ['en']
        }));
    }
    catch (e) {
        if (e instanceof Error)
            alert(e.stack || e.toString());
        else
            alert(e);
    }
}
//# sourceMappingURL=test.js.map