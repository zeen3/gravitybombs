export interface bomb {
    x: number;
    y: number;
    z: number;
    vx: number;
    vy: number;
    vz: number;
    mass: number;
    size: number;
    itime: number;
    color: string;
    id: number;
    iters: number;
}
//# sourceMappingURL=math.d.ts.map