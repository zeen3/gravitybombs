//import { OBJS } from './mco'

const LOG_UNSET = 'all: unset',
	LOG_KEY = 'color: #f4f; font-style: italic;',
	LOG_NUM = 'color: #88f',
	//LOG_STR = 'color: #8f8',
	LOG_GRE = 'color: #0f0',
	LOG_RED = 'color: #f00',
	BOMB_TRANS = 'cd',
	_1_6 = 1 / 6,
	_1_3 = 1 / 3,
	_2_3 = 2 / 3,
	TAU = 2 * Math.PI

const {abs, cos, sin, max, min, log2, sign, cbrt, hypot} = Math


let t = performance.now(),
	dt = performance.now() - t,
	d = 0,
	n = 0x7f,
	log = false,
	port: MessagePort,
	id = 0;


const Bu8c = Uint8ClampedArray.of(1, 0, 0, 0, 0, 0, 0, 0),
	Bi16 = new Int16Array(Bu8c.buffer,
		Bu8c.byteOffset, 4),
	Bu16 = new Uint16Array(Bu8c.buffer,
		Bu8c.byteOffset, 4),
	Bu32 = new Uint32Array(Bu8c.buffer,
		Bu8c.byteOffset, 2),
	UisLE = Bu32[0] === 1


const i16f: ((n: number) => number) = n => n < 0 ? n / 0x8000 : n / 0x7fff
const H2rl: ((t: number) => number) = t => {
	if (t < 0) t += 1
	if (t > 1) t -= 1
	if (t < _1_6) return 51 + 1071 * t
	if (t < 0.5) return 229
	if (t < _2_3) return 51 + 1071 * (_2_3 - t)
	return 51
}
const h2rl: ((h: number) => number) = UisLE ? h => (
	Bu8c[7] = 1,
	Bu8c[6] = H2rl(h + _1_3),
	Bu8c[5] = H2rl(h),
	Bu8c[4] = H2rl(h - _1_3),
	Bu32[1]
) : h => (
	Bu8c[4] = 1,
	Bu8c[5] = H2rl(h + _1_3),
	Bu8c[6] = H2rl(h),
	Bu8c[7] = H2rl(h - _1_3),
	Bu32[1]
)
export interface bomb {
	x: number;
	y: number;
	z: number;
	vx: number;
	vy: number;
	vz: number;
	mass: number;
	size: number;
	itime: number;
	color: string;
	id: number;
	iters: number;
}
const newexpls: number[] = [],
	expls: number[] = [],
	bombs: bomb[] = [],
	spl: bomb[] = []
const colors: {[k: number]: string} = {}
const getColor: ((k: number) => string) = k => colors[k] ||
	(colors[k] = '#' + k.toString(16).slice(1,7) + BOMB_TRANS)
const epsilon = Number.EPSILON
const mkbomb = (a: number, cx: number, cy: number): bomb => {
	crypto.getRandomValues(Bu32)
	return {
		x: cos(a) * 100 + cx,
		y: sin(a) * 100 + cy,
		z: i16f(Bi16[1]),
		vx: cos(a),
		vy: sin(a),
		vz: 0,
		mass: Bu8c[0] / 8 || epsilon,
		size: Bu8c[1] & 31 || epsilon,
		color: getColor(h2rl(Bu16[0] / 0xffff)),
		itime: performance.now(),
		id: ++id,
		iters: 0
	}
}
const portonmessage: ((e: MessageEvent) => void) = ({data}) => {
	switch (data.type) {
		case 'tick': return 'number' === typeof data.T &&
				'number' === typeof data.height&&
				'number' === typeof data.width &&
				'number' === typeof data.maxft &&
				'number' === typeof data.minft &&
				'number' === typeof data.dpr ?
				tick(data.T, data.width, data.height, cbrt(data.dpr ** 2), data.minft, data.maxft) :
				port.postMessage({
					type: 'err',
					err: 'bad args',
					args: {T: 'number', width: 'number', height: 'number'}
				})
	}
}
const OM = new Float64Array(2)
onmessage = ({data}) => {
	switch (data.type) {
		case 'click':
			'number' === typeof data.x &&
				'number' === typeof data.y &&
				'number' === typeof data.dpr &&
				newexpls.push(-1, 127, data.x * data.dpr, data.y * data.dpr)
			break;
		case 'port':
			data.port instanceof MessagePort &&
				(port = data.port) &&
				(port.onmessage = portonmessage)
			console.log(port)
			break
		case 'motion':
			if (ArrayBuffer.isView(data.f64) && data.f64 instanceof Float64Array) {
				OM[0] = data.f64[0]
				OM[1] = data.f64[1]
			}
	}
}
const NHPI = -Math.PI / 2, TAU100 = TAU / 100
let _ac = 0
let aC = () => NHPI + TAU100 * _ac++
//const anyNaN = (...a: number[]) => a.some(isNaN)
//const areNaN: ((b: bomb) => boolean) = b => anyNaN(b.vx, b.vy, b.vz, b.x, b.y, b.z)
//const OBJL: ((n: number) => string) = n => new Array(n).fill('%O').join(', ')
const tick = (T: DOMHighResTimeStamp, width = 1, height = 1, dpr = 1, minft = 1, maxft = 999) => {
	const DT = min(T - t, 51)
	dt = min(DT, 50) / 16
	if (T > t) t = T
	expls.push(...newexpls)
	newexpls.length = 0
	if (bombs.length < n)
		bombs.push(mkbomb(aC(), width / 2, height / 2))

	while (spl.length) bombs.splice(bombs.indexOf(
		spl.shift() || bombs[0]), 1)

	// gravity; three steps
	let step = 0, ix = 0, i: number, j: number, scoreadd = 0;
	const md = 1.5, l = bombs.length;
	// min d, length
	do for (i = 0; i < l; ++i) for (j = i + 1; j < l; ++j) {
		const o = bombs[i], k = bombs[j],
			dx = k.x - o.x,
			dy = k.y - o.y,
			dz = k.z - o.z
		let d = hypot(dx, dy, dz)
		if (d < md) {
			crypto.getRandomValues(Bi16)
			const ox = i16f(Bi16[ix++ & 3]),
				oy = i16f(Bi16[ix++ & 3]),
				oz = i16f(Bi16[ix++ & 3]),
				moh = md / hypot(ox, oy, oz)
			k.x += moh * ox
			k.y += moh * oy
			k.z += moh * oz
			d = md
		}
		const F = (o.mass * k.mass * 0.1) / (0.5 * d ** 2),
			Fd = F / d,
			Fx = Fd * dx,
			Fy = Fd * dy,
			Fz = Fd * dz,
			oimdt = dt / o.mass,
			ojmdt = dt / k.mass

		o.vx += Fx * oimdt
		o.vy += Fy * oimdt
		o.vz += Fz * oimdt

		k.vx += -Fx * ojmdt
		k.vy += -Fy * ojmdt
		k.vz += -Fz * ojmdt
	} while (++step < 3);
	// update; 1 step
	const e_l = expls.length, __X = OM[0], __Y = OM[1]
	for (i = 0; i < l; ++i) {
		const o = bombs[i]
		if (o.size > 48) o.mass = 0
		if (o.mass <= 0 || o.mass > 32) {
			const TT = (performance.now() | 0) - o.itime
			log && console.debug('[%s] %cremoved%c %c%s%c {%cactive%c: %c%s%c, %cmass%c: %c%s%c, %csize%c: %c%s%c}', performance.now().toPrecision(9), LOG_RED, LOG_UNSET, String.raw`color: ${o.color}`, o.color, LOG_UNSET, LOG_KEY, LOG_UNSET, TT > 1_000 ? LOG_GRE : LOG_RED, TT.toPrecision(8), LOG_UNSET, LOG_KEY, LOG_UNSET, LOG_NUM, o.mass.toPrecision(5), LOG_UNSET, LOG_KEY, LOG_UNSET, LOG_NUM, o.size.toPrecision(4), LOG_UNSET)
			newexpls.push(o.id, dpr * abs(10 * (o.mass / o.size)), o.x, o.y)
			spl.push(o)
			continue
		}
		++o.iters
		const sz = o.size / 2
		const {x, y, z, vx, vy, vz} = o
		const w = sz + width, h = sz + height
		if (x < -sz && vx < 0) o.x = w
		else if (x > w && vx > 0) o.x = -sz
		if (y < -sz && vy < 0) o.y = h
		else if (y > h && vy > 0) o.y = -sz
		if (z < -200 - sz && vz < 0) o.z = 200 - sz
		else if (z > 200 - sz && vz > 0) o.z = -200 - sz

		o.vx *= 0.99
		o.vy *= 0.99
		o.vz *= 0.99

		o.x += vx * dt
		o.y += vy * dt
		o.z += vz * dt

		o.size += 0.01
		o.mass += 0.02
		if (e_l) {
			let ei = e_l
			const mL = 15 - (log2(o.mass) * 3),
				X = o.x,
				Y = o.y
			while (--ei > 0) {
				const ey = (expls[ei--] || 0) - Y,
					ex = (expls[ei--] || 0) - X,
					de = hypot(ey, ex)

				if (de < expls[ei--]) {
					if (expls[ei] === -1) {
						scoreadd += (scoreadd | 1) >>> 0
						o.vx = -5 * ex / de
						o.vy = -5 * ey / de
					} else {
						const mLde = -mL / de
						o.vx = mLde * ex
						o.vy = mLde * ey
					}
				}
			}
		}
		const MVX = o.vx + __X, MVY = o.vy + __Y
		o.vx = abs(MVX) > 256 ? 255 * sign(MVX) : MVX
		o.vy = abs(MVY) > 256 ? 255 * sign(MVY) : MVY
	}
	port.postMessage({
		type: 'draw',
		expls: expls,
		bombs: bombs,
		scoreadd
	})
	expls.length = 0
	if (l > (n - 10) && DT < minft) {
		if (++d > 2 ) n = min(n + 1, 127)
		else if (d < 0) d = 0
	} else if (DT > maxft) {
		if (--d < -2 && l) {
			n = max(n - 1, 15)
			bombs[0].mass = -1
		} else if (d > 0) d = 0
		if (!l) n = 0x3F
	} else if (d !== sign(d)) d = 0
}
