import {bomb} from './math'

let bomber: OffscreenCanvas,
	port: MessagePort,
	ctx: OffscreenCanvasRenderingContext2D,
	width = 1,
	height = 1,
	dpr = 1,
	log = false,
	drawFPS = true,
	drawScore = false,
	font = `32px monospace`,
	score = 0,
	scoretx = '';

Object.defineProperties(self, {
	_bomber: {enumerable: true, get() {return bomber}},
	_ctx: {enumerable: true, get() {return ctx}},
	_port: {enumerable: true, get() {return port}},
	_log: {enumerable: true, get() {return (log = !log)}},
})
const {sqrt, floor, ceil, min, max} = Math
const dsptx = {format(n:number){return n.toString(10)}}
onmessage = ({data}) => {
	let tmp:Intl.NumberFormat
	switch (data.type) {
		case 'osc': data.bomber instanceof OffscreenCanvas &&
				(bomber = data.bomber) &&
				'number' === typeof data.dpr &&
				(dpr = data.dpr)
			font = `${(32 * sqrt(dpr)).toPrecision(3)}px monospace`
			console.log(bomber)
			const CTX = bomber.getContext('2d')
			if (CTX) ctx = CTX
			if (ctx && port)
				requestAnimationFrame(draw)
			break;
		case 'resize':
			if (bomber &&
				'number' === typeof data.width &&
				'number' === typeof data.height &&
				'number' === typeof data.ratio
			) {
				dpr = data.ratio
				width = bomber.width = Math.floor(data.width * dpr)
				height = bomber.height = Math.floor(data.height * dpr)
				font = `${(32 * sqrt(dpr)).toPrecision(3)}px monospace`
				console.log('resize: %dx%d with dpr %f', data.width, data.height, dpr)
			}
			break;
		case 'port':
			data.port instanceof MessagePort &&
				(port = data.port) &&
				(port.onmessage = portonmessage)
			if (port && ctx)
				requestAnimationFrame(draw)
			console.log(port)
			break
		case 'langs': Array.isArray(data.langs) &&
				(tmp = new Intl.NumberFormat(data.langs)) &&
				(dsptx.format = tmp.format) &&
				console.log('Languages set to %s (%O)', data.langs.join(', '), tmp.resolvedOptions())
			break
	}
}
let B: bomb[] = [],
	E: number[] = [];
//const init = (T: number) => port.postMessage({
//	type: 'tick',
//	buf: A,
//	T
//}, [A])
let t = performance.now(), fpstx = '', frame = 0, mframe = 0, fpsdiv = 32,
	maxft = Infinity, minft = 999
const fpsup = Number.isInteger(Math.log2(fpsdiv)) ? fpsdiv - 1 : 31,
	fpsdv = Number.isInteger(Math.log2(fpsdiv)) ? fpsdiv * 1_000 : 32_000;

const scoreSet = () => {drawScore = true}
const FPSSet = () => {mframe = 0; drawFPS = true}
const scoreClear = () => {score = 0; scoretx = ''; drawScore = false; setTimeout(FPSClear, 65535)}
const FPSClear = () => {mframe = 0; drawFPS = false}

let scoret = setTimeout(scoreSet, 999),
	fpst = setTimeout(scoreClear, 1_000);

const portonmessage: ((e: MessageEvent) => void) = ({data}) => {
	switch (data.type) {
		case 'draw':
			width = bomber.width
			height = bomber.height
			B.push.apply(B, data.bombs)
			E.push.apply(E, data.expls)
			if (data.scoreadd !== 0) {
				FPSClear()
				scoret && clearTimeout(scoret)
				fpst && clearTimeout(fpst)
				scoret = setTimeout(scoreClear, 8192)
				fpst = setTimeout(FPSSet, 12288)
				if (data.scoreadd < 0) score -= data.scoreadd
				else score += data.scoreadd
				scoretx = 'Score: ' + dsptx.format(score)
				setTimeout(scoreSet, 512)
			}
			requestAnimationFrame(draw)
			break
	}
}
const TAU = 2 * Math.PI
const u8c = Uint8ClampedArray.of(0)
const draw = (T: DOMHighResTimeStamp) => {
	port && port.postMessage({
		height: bomber.height,
		width: bomber.width,
		type: 'tick',
		minft,
		maxft,
		dpr,
		T,
	})
	const dt = T - t
	if (T > t) t = T
	u8c[0] = dt
	ctx.fillStyle = '#000000' + (u8c[0] > 31 ? u8c[0].toString(16) : '11')
	ctx.fillRect(0, 0, width, height)
	let b: bomb | undefined;
	ctx.lineCap = 'round'
	const BL = B.length
	while (b = B.pop()) {
		ctx.beginPath()
		ctx.moveTo(b.x, b.y)
		ctx.lineTo(b.x - b.vx * 3, b.y - b.vy * 3)
		let dz = b.z <= -398 ?
			0 :
			400 / (b.z - -400)
		ctx.lineWidth = b.size * dz * dpr
		ctx.fillStyle = ctx.strokeStyle = b.color
		ctx.stroke()
		ctx.fill()
	}
	ctx.fillStyle = '#fff'
	while (E.length) {
		const y = E.pop() || 0,
			x = E.pop() || 0,
			r = E.pop() || 0,
			i = E.pop() || 0;
		ctx.beginPath()
		ctx.arc(x, y, isFinite(r) ? r / 2 : max(width, height), 0, TAU, true);
		ctx.fill()
		log && console.debug(
			'Explosion at %d,%d with radius %d; init %i',
			x, y, r, i)
	}
	ctx.font = font
	ctx.textAlign = 'left'
	ctx.textBaseline = 'top'
	ctx.fillStyle = '#000a'
	ctx.strokeStyle = '#fffa'
	ctx.lineCap = 'square'
	ctx.lineWidth = 2
	frame++
	frame &= fpsup
	if (!frame) {
		if (mframe !== 0) {
			const ft = T - mframe
			fpstx = 'FPS: ' + dsptx.format(fpsdv / ft)
			maxft = floor(min(ft / fpsdiv, maxft - 1)) + 1
			minft = ceil(min(ft / fpsdiv, minft - 1)) + 1
		}
		mframe = T
		//const tm = ctx.measureText(fpstx)
		//txw = tm.width
		//txh = 34
	}
	if (drawFPS) {
		//ctx.fillRect(0, 0, txw, txh)
		const tx = `[${BL.toString()}] ${fpstx}`
		ctx.strokeText(tx, 0, 0)
		ctx.fillText(tx, 0, 0)
	} else if (drawScore) {
		const tx = `[${BL.toString()}] ${scoretx}`
		ctx.strokeText(tx, 0, 0)
		ctx.fillText(tx, 0, 0)
	}
}
