#!/bin/make

all: shit esnext amd .mkln

esnext:
	tsc -t ESNext -m ESNext
	for v in public/*.js; do \
		sed \
			-e 's/.js.map/.mjs.map/g' \
			-e "s/from \([\"']\)\\(.\/[^\\1]*\\)\\1/from \\1\\2.mjs\\1/g" \
			-e "s/from \([\"']\)\\(..\/[^\\1]*\\)\\1/from \\1\\2.mjs\\1/g" \
			-e "s/import(\(['\"]\)\([^\1\]\)\1)/import(\\1\\2.mjs\\1)/g" \
			-i "$$v" ; \
		mv "$$v" "$$(dirname "$$v")/$$(basename "$$v" .js).mjs" ; done
	for v in public/*.js.map; do \
		mv "$$v" "$$(dirname "$$v")/$$(basename "$$v" .js.map).mjs.map" ; done

umd:
	tsc -m umd -t ES2018

amd:
	tsc -m amd -t ES2018

cjs:
	tsc -m commonjs -t ES2017

none:
	tsc -m none 

.mkln:
	for v in public/*.js public/*.mjs public/*js.map; do if [[ -f "$$v" ]]; then ln -sf "$$v" "./$$(basename "$$v")"; fi; done;

clrsm:
	if [[ -d public ]]; then rm -r public; fi
	symlinks -rd .

shit:
	mkdir -p public
	cp -u gravity.html public/index.html

git:
	git add .
	git commit -m make
	git push

commit: all git

include clrsm
